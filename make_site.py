#!venv/bin/python
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:08:53 2016

@author: kevin
"""

import argparse, distutils, os, shutil, json, stat
from jinja2 import Environment, FileSystemLoader

ASSETS_FOLDER = "assets"
OUTPUT_FOLDER = "output"
TEMPLATES_FOLDER = "templates"

DATA_FILE = ".data"
EXCLUDE_FILE = ".exclude"

EXCLUDE_MISSING_MSG = \
    "Warning: Exclude file not found at '%s', continuing without..."

def load_data(site_path):
    data = dict()
    data_path = os.path.join(site_path, DATA_FILE)
    try:
        os.stat(data_path)
        with open(data_path, "rU") as data_file:
            data = json.load(data_file)
    except OSError as os_error:
        exit(os_error)
    return data
        
def load_excludes(site_path):
    excludes = []
    exclude_path = os.path.join(site_path, EXCLUDE_FILE)
    try:
        os.stat(exclude_path)
        with open(exclude_path, "rU") as exclude_file:
            for line in exclude_file:
                excludes.append(line.rstrip('\n'))
    except OSError as os_error:
        if os_error.errno == 2:
            print(EXCLUDE_MISSING_MSG % exclude_path)
        else:
            exit(os_error)
    return excludes

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description="""\
    Builds a web site from templates; the required structure is as follows:
    
    <site-path> (should contain .data and .exclude files)
    <site-path>/%s (copied unmodified to output directory)
    <site-path>/%s (HTML templates)
    <site-path>/%s (output directory created)
    """ % (ASSETS_FOLDER, TEMPLATES_FOLDER, OUTPUT_FOLDER))
parser.add_argument("site-path")
parser.add_argument("-o", "--output", nargs='?', const="missing",
                    default=OUTPUT_FOLDER,
                    help="output directory (default=%s)" % OUTPUT_FOLDER)
parser.add_argument("-D", nargs='+', default=[],
                    help="defined variables (name=value name=value ...) to be\
                        passed to all templates")
parser.add_argument("-v", "--verbose", action="store_true",
                    help="verbose output")
args = parser.parse_args()
name_space = vars(args)
output_folder = name_space['output']
if output_folder == "missing":
    exit("Output directory specified but not provided: -o")
site_path = name_space["site-path"]
template_path = os.path.join(site_path, TEMPLATES_FOLDER)
assets_path = os.path.join(site_path, ASSETS_FOLDER)
for path in [site_path, template_path, assets_path]:
    try:
        st = os.stat(path)
    except OSError as os_error:
        exit(os_error)
    if not stat.S_ISDIR(st.st_mode):
        exit("Not a directory: '%s'" % path)
try:
    data = load_data(site_path)
except Exception as e:
    print("Problem with .data file, unable to continue...")
    exit(e)
for item in name_space['D']:
    bits = item.split('=')
    if len(bits) == 2:
        try:
            bits[1] = distutils.util.strtobool(bits[1])
        except ValueError:
            pass
        data[bits[0]] = bits[1]
excludes = load_excludes(site_path)
output_path = os.path.join(site_path, output_folder)
if args.verbose:
    print("Clearing output directory: '%s'" % output_path)
shutil.rmtree(output_path, True)
if args.verbose:
    print("Copying assets from '%s' to '%s'" % (assets_path, output_path))
shutil.copytree(assets_path, output_path)
env = Environment(loader=FileSystemLoader(template_path),
                  extensions=["jinja2.ext.with_"])
template_list = env.list_templates(extensions=["html"])
if len(template_list) == 0:
    exit("No templates found: '%s'" % template_path)
for template_name in template_list:
    if template_name in excludes:
        if args.verbose:
            print("Skipping '%s'..." % template_name)
    else:
        print("Processing '%s'..." % template_name)
        if '/' in template_name:
            dir_name = '/'.join(template_name.split('/')[:-1])
            dir_name = os.path.join(output_path, dir_name)
            if not os.path.exists(dir_name):
                os.mkdir(dir_name)
        template = env.get_template(template_name)
        template.stream(data).dump(os.path.join(output_path, template_name))
print("Processing complete")
