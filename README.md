# README #

### What is this repository for? ###

* A small Python (2.7) program for creating static web sites using the Jinja2 template language.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Clone the repository:

    git clone https://kejoro@bitbucket.org/kejoro/site_maker.git

1. Run the setup script:

    ./setup.sh

### How do I use it? ###

SiteMaker combines templates and data to generate a static web site; it expects the following directory structure:

    <site-path>            (this should contain files called .data and .exclude)
    <site-path>/assets     (contents are copied unmodified to the output directory)
    <site-path>/templates  (HTML Jinja2 templates with '.html' suffix)
    <site-path>/output     (the output directory, deleted and re-created on each execution)

The .exclude file should contain one template name per line - these templates are not processed.

The .data file should contain a JSON data structure - this can be referenced in the templates.

Use `./make_site.py -h` for help.

### Who do I talk to? ###

* [Kevin Rowley](mailto:KevinRowley@acm.org)
